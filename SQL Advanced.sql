
-- Ejercicio 1
-- Nombre y Apellido de los profesores del departamento de Lenguajes

select p.nombre, apellido1, IFNULL(apellido2, '') 'apellido 2'
from profesores p join departamentos d on (p.departamento = d.codigo)
where upper(d.nombre) like '%LENGUAJES%';


-- Ejercicio 2
/*Usando la función NVL extraiga un listado con el código y el nombre de las asignaturas
de las que está matriculado 'Nicolas Bersabe Alba'. Proporcione además el número de
créditos prácticos, pero caso de ser nulo, debe salir "No tiene" en el listado. */

select a.nombre, a.APELLIDO1, a.APELLIDO2, asig.NOMBRE, ifnull(asig.CREDITOS, 'No tiene')
from alumnos a, matricular m, asignaturas asig
where a.DNI = m.alumno and m.ASIGNATURA= asig.CODIGO 
and upper(a.nombre) like 'NICOLAS' and upper(a.apellido1) like 'BERSABE' and upper(a.apellido2) like 'ALBA';


-- Ejercicio 3
-- ALUMNOS QUE TENGA APROGADOS LA ASIGNATURA BASE DE DATOS

select a.NOMBRE, a.APELLIDO1
from alumnos a, matricular m, asignaturas ag
where m.CALIFICACION is not null and m.CALIFICACION != 'SP'
and m.ALUMNO = a.DNI  and m.ASIGNATURA = ag.CODIGO
and upper(ag.NOMBRE) = 'BASES DE DATOS';


/* Ejercicio 4
Obtenga un listado en el que aparezcan el identificador de los profesores, su nombre y
apellidos, así como el código de las asignaturas que imparte y su nombre.
*/ 
select distinct p.id, p.nombre, p.APELLIDO1, p.APELLIDO2, a.CODIGO, a.NOMBRE
from profesores p, impartir i, asignaturas a
where p.ID= i.PROFESOR and i.ASIGNATURA = a.CODIGO;



/* Ejercicio 5 PREGUNTAR
Obtener el nombre de cada profesor junto con el de su director de tesis. 
En caso de no tener director de tesis en el listado debe aparecer “No tiene”
*/

select p1.NOMBRE, p1.APELLIDO1, p1.APELLIDO2, ifnull(concat(p2.nombre, ' ', p2.APELLIDO1), 'No tiene') 'Director tesis'
from profesores p1 left outer join profesores p2 on (p1.DIRECTOR_TESIS = p2.ID);

/* Ejercicio 6
Nombre y edad de parejas de alumnos que tengan el mismo primer apellido. 
Téngase en cuenta que los apellidos podrían estar en mayúscula o en minúscula.
*/

select a1.nombre, ifnull(timestampdiff(year, a1.FECHA_NACIMIENTO, sysdate()), 'desconocida') as edad ,a2.nombre, ifnull(timestampdiff(year, a2.FECHA_NACIMIENTO, sysdate()), 'desconocido') as edad,a1.APELLIDO1 
from alumnos a1, alumnos a2
where upper(a1.APELLIDO1) = upper(a2.APELLIDO1) and upper(a1.DNI)< upper(a2.DNI);

/* Ejercicio 7
Combinaciones de apellidos que se pueden obtener con los primeros apellidos de
alumnos nacidos entre los años 1995 y 1996, ambos incluidos. Para obtener el año usar
la función YEAR. Se recomienda utilizar el operador BETWEEN … AND … para expresar el
rango de valores. */

select a1.apellido1, a2.APELLIDO1
from alumnos a1, alumnos a2
where year(a1.FECHA_NACIMIENTO) between 1995 and 1996
and year(a2.FECHA_NACIMIENTO) between 1995 and 1996 
and a1.DNI > a2.DNI;

/* Ejercicio 8
Nombre y apellidos de parejas de profesores cuya diferencia de antigüedad (en valor
absoluto) sea inferior a dos años y pertenezcan al mismo departamento. Muestre la
antigüedad de cada uno de ellos en años. Usar la función TIMESTAMPDIFF.
*/

select  concat(p1.nombre, ' ', p1.APELLIDO1) 'Nombre y apellido 1', concat(p2.nombre, ' ', p2.APELLIDO1) 'Nombre y apellido 2'
from profesores p1, profesores p2
where p1.ID > p2.ID
and abs(timestampdiff(year, p1.ANTIGUEDAD, sysdate()) - timestampdiff(year, p2.ANTIGUEDAD, sysdate())) < 2
AND p1.DEPARTAMENTO = P2.DEPARTAMENTO;

/* Ejercicio 9
Construya un listado en el que se muestren todos los posibles emparejamientos
heterosexuales que se pueden formar entre los alumnos matriculados en la asignatura
de código 112 donde la nota de la mujer es mayor que la del hombre y ambos se
matricularon en la misma semana. En el listado muestre primero el nombre de la mujer
y a continuación el del hombre. Etiquete las columnas como "Ella" y "El"
respectivamente. Para el cálculo de la semana use la función de conversión WEEK. Para
el cálculo de la nota numérica use 1 para suspenso, 2 para aprobado, 3 para notable, 4
para sobresaliente, 5 para matrícula de honor y 0 para las notas a null con la función
case.
*/

SELECT DISTINCT tabla1.chica, tabla2.chico FROM (
SELECT CONCAT (a.NOMBRE, ' ', a.APELLIDO1, ' ', IFNULL(a.APELLIDO2,' ')) AS chica, a.FECHA_PRIM_MATRICULA,
CASE 
  WHEN m.CALIFICACION = 'SP' THEN 1 
  WHEN m.CALIFICACION = 'AP' THEN 2
        WHEN m.CALIFICACION = 'NT' THEN 3
        WHEN m.CALIFICACION = 'SB' THEN 4
        WHEN m.CALIFICACION = 'MH' THEN 5
        ELSE '0' END puntuacion  
FROM alumnos a, asignaturas ag, matricular m
WHERE ag.CODIGO = '112' AND a.GENERO = 'FEM' AND ag.CODIGO = m.ASIGNATURA AND m.ALUMNO = a.DNI) tabla1,
(SELECT DISTINCT CONCAT (a.NOMBRE, ' ', a.APELLIDO1, ' ', IFNULL(a.APELLIDO2,' ')) AS chico, a.FECHA_PRIM_MATRICULA,
CASE 
  WHEN m.CALIFICACION = 'SP' THEN 1 
  WHEN m.CALIFICACION = 'AP' THEN 2
        WHEN m.CALIFICACION = 'NT' THEN 3
        WHEN m.CALIFICACION = 'SB' THEN 4
        WHEN m.CALIFICACION = 'MH' THEN 5
        ELSE '0' END puntuacion  
 FROM alumnos a, asignaturas ag, matricular m
WHERE ag.CODIGO = '112' AND a.GENERO = 'MASC' AND ag.CODIGO = m.ASIGNATURA AND m.ALUMNO = a.DNI) tabla2
WHERE ABS(TIMESTAMPDIFF(WEEK, tabla1.FECHA_PRIM_MATRICULA, tabla2.FECHA_PRIM_MATRICULA)) < 1 AND tabla1.puntuacion > tabla2.puntuacion;


/* Ejercicio 10
Tríos de asignaturas pertenecientes a la misma materia. Debe presentarse el nombre de
las 3 asignaturas seguido del código de la materia a la que pertenecen.
*/

select distinct a1.nombre, a2.nombre, a3.nombre, a1.COD_MATERIA, a2.COD_MATERIA, a3.COD_MATERIA
from asignaturas a1, asignaturas a2, asignaturas a3
where a1.CODIGO < a2.CODIGO and a2.CODIGO < a3.CODIGO and a1.CODIGO < a3.CODIGO
and a1.COD_MATERIA = a2.COD_MATERIA and a2.COD_MATERIA = a3.COD_MATERIA;



/* Ejercicio 11
Muestre el nombre, apellidos, nombre de la asignatura y las notas obtenidas por todos
los alumnos con más de 22 años. Utilice la función DECODE para mostrar la nota como
(Matricula de Honor, Sobresaliente, Notable, Aprobado, Suspenso o No Presentado).
Ordene por apellidos y nombre del alumno. */

select a.NOMBRE, apellido1, APELLIDO2, ag.NOMBRE,
case
	when m.CALIFICACION = 'AP' then 'Aprobado'
    when m.CALIFICACION = 'NT' then 'Notable'
    when m.CALIFICACION = 'SB' then 'Sobresaliente'
    when m.CALIFICACION = 'SP' then 'Suspenso'
    else 'No presentado' end as nota
from alumnos a, matricular m,asignaturas ag
where timestampdiff(year, a.FECHA_NACIMIENTO, sysdate())> 22
and m.ALUMNO=a.DNI and m.ASIGNATURA = ag.CODIGO
order by APELLIDO1, APELLIDO2, a.nombre;


/* Ejercicio 12
Nombre y apellidos de todos los alumnos a los que les de clase Enrique Soler. Tenga en
cuenta que hay que utilizar los atributos ASINGNATURA, GRUPO y CURSO de las tablas
IMPARTIR y MATRICULAR. Cada alumno debe aparecer una sola vez. Ordénelos por
apellidos, nombre.*/



select alumnos.nombre, alumnos.apellido1, alumnos.apellido2
from (impartir natural join matricular)  join profesores on (impartir.PROFESOR = profesores.ID)
join alumnos on (matricular.alumno= alumnos.DNI)
where upper(profesores.nombre) = 'ENRIQUE' AND UPPER(profesores.apellido1) = 'SOLER'
order by alumnos.apellido1, alumnos.apellido2,alumnos.nombre;

/* Ejercicio 13
Nombre y apellidos de los alumnos matriculados en asignaturas impartidas por
profesores del departamento de 'Lenguajes y Ciencias de la Computación'. El listado
debe estar ordenado alfabéticamente.
*/

select  alumnos.nombre, alumnos.apellido1, alumnos.apellido2
from (impartir natural join matricular)  join profesores on (impartir.PROFESOR = profesores.ID)
join alumnos on (matricular.alumno= alumnos.DNI), departamentos
where profesores.DEPARTAMENTO = departamentos.CODIGO and  upper(departamentos.nombre) LIKE '%LENGUAJES%'
order by alumnos.apellido1, alumnos.apellido2,alumnos.nombre;

/* Ejercicio 14
Listado con el nombre de las asignaturas, nombre de la materia a la que pertenece y
nombre, apellidos y carga de créditos de los profesores que la imparten. El listado debe
estar ordenado por código de materia y por orden alfabético inverso del nombre de
asignatura. Usar la función concat para mostrar el nombre completo del profesor en una
sola columna.*/

select  a.nombre, d.NOMBRE, concat(p.NOMBRE, ' ',p.APELLIDO1, ' ', ifnull(p.APELLIDO2, '')) 'Profesor', i.CARGA_CREDITOS
from asignaturas a, impartir i, profesores p, departamentos d
where a.CODIGO= i.ASIGNATURA and i.PROFESOR = p.id and a.departamento = d.codigo;

/* Ejercicio 15
Listado con el nombre de asignatura, nombre de departamento al que está asignada,
total de créditos y porcentaje de créditos prácticos, ordenado decrecientemente por el
porcentaje de créditos prácticos. Aquellas asignaturas cuyo número de créditos totales,
prácticos o teóricos no está especificado no deben salir en el listado.
*/
select a.nombre, d.nombre, a.CREDITOS, concat((ifnull(a.PRACTICOS, 0)/a.CREDITOS)*100, '%') 'Porcentaje créditos prácticos'
from asignaturas a, departamentos d
where a.DEPARTAMENTO = d.CODIGO and a.CREDITOS is not null and a.PRACTICOS is not null and a.TEORICOS is not null
order by a.CREDITOS desc;

/* Ejercicio 16
Utilice las operaciones de conjuntos para buscar alumnos que puedan ser familia de
algún profesor, es decir, su primer o segundo apellido es el mismo que el primer o
segundo apellido de un profesor, aunque no necesariamente en el mismo orden.
Muestre simplemente los apellidos comunes. */



select alumnos.APELLIDO1 'Apellido'
from alumnos, profesores
where upper(alumnos.APELLIDO1) = upper(profesores.APELLIDO1) or alumnos.APELLIDO1 = profesores.APELLIDO2
union
select alumnos.APELLIDO2
from alumnos, profesores
where alumnos.APELLIDO2 = profesores.APELLIDO1 or alumnos.APELLIDO2 = profesores.APELLIDO2
union
select profesores.APELLIDO1
from alumnos, profesores
where upper(alumnos.APELLIDO1) = upper(profesores.APELLIDO1) or alumnos.APELLIDO2 = profesores.APELLIDO1
union
select profesores.APELLIDO2
from alumnos, profesores
where upper(alumnos.APELLIDO1) = upper(profesores.APELLIDO2) or alumnos.APELLIDO2 = profesores.APELLIDO2;


/* Ejercicio 17
Apellidos que contienen la letra elle ( 'll' ) tanto de alumnos como de profesores
 */
select a.APELLIDO1
from alumnos a
where a.APELLIDO1 like '%ll%'
union
select a.APELLIDO2
from alumnos a
where a.APELLIDO2 like '%ll%'
union
select p.APELLIDO1
from profesores p
where p.APELLIDO1 like '%ll%'
union
select p.APELLIDO2
from profesores p
where p.APELLIDO2 like '%ll%';



/* Ejercicio 18
Busque una incongruencia en la base de datos, es decir, asignaturas en las que el
número de créditos teóricos + prácticos no sea igual al número de créditos totales.
Muestre también los profesores que imparten esas asignaturas. */

select distinct a.NOMBRE, i.PROFESOR
from asignaturas a left join impartir i
on i.ASIGNATURA = a.CODIGO
where (ifnull(a.TEORICOS, 0) + ifnull(a.PRACTICOS, 0)) != a.CREDITOS;


/* Ejercicio 19
Muestre en orden alfabético los nombres completos de todos los profesores y a su lado
el de sus directores si es el caso (si no tenemos constancia de su director de tesis
dejaremos este espacio en blanco, pero el profesor debe aparecer en el listado).
*/

select  concat(p1.APELLIDO1, ' ', ifnull(p1.APELLIDO2, ''), ', ',p1.nombre) as Nombre_Completo, concat(p2.APELLIDO1, ' ', p2.APELLIDO2, ', ',p2.nombre) as Director_Tesis
from profesores p1 left outer join profesores p2 
on (p1.DIRECTOR_TESIS = p2.ID)
order by Nombre_Completo;


/* Ejercicio 20
Muestre el nombre y apellidos de cada profesor junto con los de su director de tesis y
el número de tramos de investigación del director. Recuerde que el director de tesis de
un profesor viene dado por el atributo DIRECTOR_TESIS y el número de tramos se
encuentra en la tabla INVESTIGADORES. Los nombres de cada profesor y su director
deben aparecer con el siguiente formato: 'El Director de Angel Mora Bonilla es Manuel
Enciso Garcia-Oliveros'
*/

select  concat('El director de ', p1.nombre, ' ', p1.APELLIDO1, ' ', ifnull(p1.APELLIDO2, ''), ' es ',
p2.nombre, ' ', p2.APELLIDO1, ' ', ifnull(p2.APELLIDO2, '')) 'Profesor-Director', i.TRAMOS
from profesores p1 left outer join profesores p2 on (p1.DIRECTOR_TESIS = p2.ID), investigadores i
where p2.id = i.ID_PROFESOR;

/* Ejercicio 21
Liste el nombre de todos los alumnos ordenados alfabéticamente. Si dicho alumno
tuviese otro alumno que se ha matriculado exactamente a la vez que él, muestre el
nombre de este segundo alumno a su lado.
*/

SELECT nombre, apellido1, apellido2, null 'Nombre_A2', null 'Apellido1_A2', null 'Apellido2_A2'
FROM alumnos
WHERE dni NOT IN(
	SELECT a1.dni
	FROM (alumnos a1 join alumnos a2 on a1.fecha_prim_matricula=a2.fecha_prim_matricula)
	WHERE a1.dni != a2.dni)
UNION
SELECT a1.nombre, a1.apellido1, a1.apellido2, a2.nombre, a2.apellido1, a2.apellido2
FROM (alumnos a1 join alumnos a2 on a1.fecha_prim_matricula=a2.fecha_prim_matricula)
WHERE a1.dni != a2.dni
ORDER BY apellido1, apellido2, nombre;


/* Ejercicio 22
Listado con el nombre de todas las asignaturas. En caso de que exista, para cada
asignatura se muestra el curso, grupo y nombre y primer apellido del profesor que la
imparte.
*/
select distinct a.NOMBRE, ifnull(a.curso,'') 'Curso', ifnull(i.grupo,'') 'Grupo', ifnull(concat(p.nombre, ' ', p.APELLIDO1),'') 'Nombre Profesor'
from (asignaturas a left outer join impartir i on (a.CODIGO = i.ASIGNATURA)) left outer join profesores p on i.PROFESOR= p.id;








