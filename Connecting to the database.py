import pymysql.cursors  
 
# Connect to the database.
connection = pymysql.connect(host='localhost',
                             user='root',
                             password='master',                             
                             db='docencia',
                             charset='utf8mb4',
                             cursorclass=pymysql.cursors.DictCursor)
 
print ("connect successful!!")
 
try:
  
    #Abrimos un cursor
    with connection.cursor() as cursor:
        
        #Escribimos la sentencia
        sql =  "insert into profesores (id, nombre, apellido1, apellido2, departamento, telefono, email, despacho, FECHA_NACIMIENTO, ANTIGUEDAD, DIRECTOR_TESIS) values ('D-18-PC-18', 'Mohammed', 'Ali', null, 1, null, 'moha.ali@uma.es', null, str_to_date('17-01-1942', '%d-%m-%Y'), null, null)"
       
        #ejecutamos la sentencia y guardamos el resultado en el cursor
        cursor.execute(sql) 
        
        #hacemos un commit para que la insercción sea definitiva
        connection.commit() 
        
        #Creamos la consulta SQL 
        sql = "select * from profesores where profesores.DEPARTAMENTO=1"
         
        # Ejecutamos la consulta
        cursor.execute(sql)
         
        
        # Para pintar una linea en blanco
        print()
 
        for row in cursor:
            print(row)
             
finally:
    # Close connection.
    connection.close()