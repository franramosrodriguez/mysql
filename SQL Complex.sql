# Ejercicio 1
# Liste el nombre y código de las asignaturas que tienen en su mismo curso otra con más
# créditos que ella

select a1.NOMBRE, a1.CODIGO
from asignaturas a1
where exists (	select *
		from asignaturas a2
        where a1.CURSO = a2.curso
        and ifnull(a1.CREDITOS,0) < ifnull(a2.CREDITOS,0));
        
# Ejercicio 2
# Listar el nombre de las asignaturas que tienen más créditos que TODAS las asignaturas
# de segundo curso

select a1.nombre
from asignaturas a1
where a1.CREDITOS > all (	select ifnull(a2.CREDITOS, 0)
							from asignaturas a2
							where a2.curso = 2);
                            
select a2.CREDITOS
from asignaturas a2
where a2.curso = 2;


/* Ejercicio 3
Mostrar el código de las asignaturas que imparte el profesor Manuel Enciso, usando una
subconsulta.
*/
select *
from asignaturas a, impartir i
where a.CODIGO= i.ASIGNATURA and i.PROFESOR=(

						select p.id
						from profesores p
						where upper(p.nombre)= 'MANUEL' and upper(p.APELLIDO1) = 'ENCISO');
                    

/* Ejercicio 4
Mostrar el código de los alumnos que reciben clase del profesor de código C-34-TU-00,
usando una subconsulta.
*/

select m.ALUMNO
from matricular m, asignaturas ag, impartir i
where m.ASIGNATURA= ag.CODIGO and i.ASIGNATURA= ag.CODIGO and i.profesor=(

										select distinct i.profesor
										from impartir i
										where i.PROFESOR='C-34-TU-00');
 
# Ejercicio 5
# Calcular el número de profesores de cada departamento. Muestre el nombre del departamento y el número de profesores.

select d.NOMBRE, count(p.ID)
from profesores p, departamentos d
where p.DEPARTAMENTO = d.CODIGO
group by d.CODIGO, d.nombre;

 
 #Ejercicio 6
/* Calcular el número de créditos asignados a cada departamento. Se consideran los
créditos establecidos para cada asignatura del departamento, no si la imparten o no
profesores del mismo, es decir, sume directamente los créditos de las asignaturas y
reúna asignaturas y departamentos directamente, sin utilizar ninguna otra tabla */

select d.nombre, sum(asig.CREDITOS) 'CREDITOS TOTALES'
from departamentos d, asignaturas asig
where d.CODIGO = asig.DEPARTAMENTO
group by d.codigo, d.nombre;

/* Ejercicio 7
Calcular el número de alumnos matriculados por curso (cada alumno debe contar una
sola vez por curso, aunque esté matriculado de varias asignaturas). Utilice COUNT
(DISTINCT ...).
*/
select count(distinct alumno), CURSO
from matricular
group by curso;

/* Ejercicio 8
Por cada número de despacho, indicar el total de créditos impartidos por profesores
ubicados en ellos.
*/
select count(i.CARGA_CREDITOS), p.DESPACHO
from profesores p, impartir i
where p.id= i.profesor
group by DESPACHO;

/* Ejercicio 9
Calcular, por cada asignatura, qué porcentaje de sus alumnos son mujeres. Mostrar el
código de la asignatura y el porcentaje. Usar la función NULLIF.
*/
select count(a.dni), m.ASIGNATURA, a.GENERO
from alumnos a, matricular m
where a.dni = m.ALUMNO
group by m.ASIGNATURA, genero;


#Ejercicio 11.
/*Visualizar, por cada departamento, el nombre del profesor más cercano a la jubilación
(de mayor edad). */

select d.nombre, concat(p.nombre, ' ', p.APELLIDO1)
from profesores p, departamentos d
where p.DEPARTAMENTO = d.CODIGO and (d.nombre, p.FECHA_NACIMIENTO) in (
		select d.nombre, min(p.FECHA_NACIMIENTO)
		from departamentos d, profesores p
		where d.codigo = p.departamento
		group by d.nombre, d.codigo);
        
        
#Ejercicio 12
# Visualizar la asignatura de mayor número de créditos en que se ha matriculado cada alumno

#Para cad alumno la asignatura que más créditos tiene

select distinct a.nombre, a.APELLIDO1, asig.nombre
from asignaturas asig, matricular m, alumnos a
where m.ASIGNATURA = asig.CODIGO and m.ALUMNO = a.DNI and (a.dni, asig.CREDITOS) in
											(select m.ALUMNO, max(asig.CREDITOS)
											from matricular m, asignaturas asig
											where  m.asignatura = asig.codigo
											group by m.alumno);








#Ejercicio 16.
/*Visualizar el profesor con mayor carga de créditos. Considere la carga de créditos como
la suma de los créditos de las asignaturas que imparte dicho profesor. Nota: Tenga en
cuenta que un profesor puede impartir sólo una parte de una asignatura, por lo que se
debe utilizar los créditos de la tabla impartir. */
                    

select p.NOMBRE, p.APELLIDO1, sum(i.CARGA_CREDITOS)
from impartir i, profesores p
where i.profesor = p.id
group by p.id, p.nombre, p.APELLIDO1
having sum (i.CARGA_CREDITOS) =
(select max(c.carga) from
(select sum(impartir.CARGA_CREDITOS) 'CARGA' from impartir group by PROFESOR) as c);

#Ejercicio 18
select imp.profesor, sum(imp.CARGA_CREDITOS)
from impartir imp
group by imp.PROFESOR
having sum(imp.CARGA_CREDITOS) <10;

# Ejercicio 21

#. Dar el nombre de las asignaturas hueso. Una asignatura se dice hueso si ningún alumno
#la ha superado.

select a1.NOMBRE
from asignaturas a1
where a1.codigo not in (
		select m.asignatura
		from matricular m
		where m.calificacion != 'SP' or m.CALIFICACION is not null);
        
#Ejercicio 22
# Listar el nombre de los departamentos que no tienen ninguna asignatura con más de 6 créditos. 


select d.nombre
from departamentos d
where d.CODIGO not in (
					select a.departamento
					from asignaturas a
					where ifnull(a.creditos,0) > 6);









