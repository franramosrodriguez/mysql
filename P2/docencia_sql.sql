-- MySQL Script generated by MySQL Workbench
-- Wed Oct 24 15:21:08 2018
-- Model: New Model    Version: 1.0
-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema docencia_relacional
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema docencia_relacional
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `docencia_relacional` DEFAULT CHARACTER SET utf8 ;
USE `docencia_relacional` ;

-- -----------------------------------------------------
-- Table `docencia_relacional`.`provincia`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `docencia_relacional`.`provincia` (
  `CODIGO INT(2)` INT(2) NOT NULL,
  `NOMBRE` VARCHAR(30) NULL,
  PRIMARY KEY (`CODIGO INT(2)`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `docencia_relacional`.`municipios`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `docencia_relacional`.`municipios` (
  `CPRO` INT(2) NOT NULL,
  `CMUN` INT(5) NOT NULL,
  `NOMBRE` VARCHAR(50) NOT NULL,
  `HOMBRES` INT(11) NULL,
  PRIMARY KEY (`CMUN`, `CPRO`),
  CONSTRAINT `CPRO_Municipios`
    FOREIGN KEY (`CPRO`)
    REFERENCES `docencia_relacional`.`provincia` (`CODIGO INT(2)`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `docencia_relacional`.`alumnos`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `docencia_relacional`.`alumnos` (
  `DNI` INT(9) NOT NULL,
  `NOMBRE` VARCHAR(20) NOT NULL,
  `APELLIDO1` VARCHAR(20) NULL,
  `APELLIDO2` VARCHAR(20) NULL,
  `GENERO` VARCHAR(4) NULL,
  `DIRECCION` VARCHAR(100) NULL,
  `TELEFONO` VARCHAR(9) NULL,
  `EMAIL` VARCHAR(40) NULL,
  `FECHA_NACIMIENTO` DATE NULL,
  `FECHA_PRIM_MATRICULA` DATE NULL,
  `CPRO` INT(2) NULL,
  `CMUN` INT(5) NULL,
  PRIMARY KEY (`DNI`),
  INDEX `CPRO_idx` (`CPRO` ASC, `CMUN` ASC),
  CONSTRAINT `CPRO`
    FOREIGN KEY (`CPRO` , `CMUN`)
    REFERENCES `docencia_relacional`.`municipios` (`CPRO` , `CMUN`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `docencia_relacional`.`departamentos`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `docencia_relacional`.`departamentos` (
  `CODIGO` INT(3) NOT NULL,
  `NOMBRE` VARCHAR(40) NOT NULL,
  `DIRECCION` VARCHAR(30) NULL,
  `TELEFONO` VARCHAR(9) NULL,
  `WEB` VARCHAR(30) NULL,
  `FECHA_CREACION` DATE NULL,
  PRIMARY KEY (`CODIGO`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `docencia_relacional`.`materias`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `docencia_relacional`.`materias` (
  `CODIGO` INT(2) NOT NULL,
  `NOMBRE` VARCHAR(40) NULL,
  `DESCRIPCION` VARCHAR(200) NULL,
  PRIMARY KEY (`CODIGO`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `docencia_relacional`.`asignaturas`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `docencia_relacional`.`asignaturas` (
  `CODIGO` INT(3) NOT NULL,
  `NOMBRE` VARCHAR(50) NOT NULL,
  `DEPARTAMENTO` INT(3) NULL,
  `CREDITOS` DECIMAL(3,1) NULL,
  `TEORICOS` DECIMAL(3,1) NULL,
  `PRACTICOS` DECIMAL(3,1) NULL,
  `CARACTER` VARCHAR(2) NULL,
  `CURSO` INT(1) NULL,
  `WEB` VARCHAR(30) NULL,
  `COD_MATERIA` INT(2) NULL,
  PRIMARY KEY (`CODIGO`),
  INDEX `DEPARTAMENTO_idx` (`DEPARTAMENTO` ASC),
  INDEX `COD_MATERIA_idx` (`COD_MATERIA` ASC),
  CONSTRAINT `DEPART`
    FOREIGN KEY (`DEPARTAMENTO`)
    REFERENCES `docencia_relacional`.`departamentos` (`CODIGO`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `COD_MAT`
    FOREIGN KEY (`COD_MATERIA`)
    REFERENCES `docencia_relacional`.`materias` (`CODIGO`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `docencia_relacional`.`correquisitos`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `docencia_relacional`.`correquisitos` (
  `ASIGNATURA` INT(3) NOT NULL,
  `CORREQUISITO` INT(3) NOT NULL,
  PRIMARY KEY (`ASIGNATURA`, `CORREQUISITO`),
  INDEX `CORREQUISITO_idx` (`CORREQUISITO` ASC),
  CONSTRAINT `ASIG`
    FOREIGN KEY (`ASIGNATURA`)
    REFERENCES `docencia_relacional`.`asignaturas` (`CODIGO`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `CORRE_ASIG`
    FOREIGN KEY (`CORREQUISITO`)
    REFERENCES `docencia_relacional`.`asignaturas` (`CODIGO`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `docencia_relacional`.`profesores`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `docencia_relacional`.`profesores` (
  `ID` VARCHAR(20) NOT NULL,
  `NOMBRE` VARCHAR(20) NOT NULL,
  `APELLIDO1` VARCHAR(20) NOT NULL,
  `APELLIDO2` VARCHAR(20) NULL,
  `DEPARTAMENTO` INT(3) NOT NULL,
  `TELEFONO` VARCHAR(4) NULL,
  `EMAIL` VARCHAR(100) NULL,
  `DESPACHO` VARCHAR(10) NULL,
  `FECHA_NACIMIENTO` DATE NULL,
  `ANTIGUEDAD` DATE NULL,
  `DIRECTOR_TESIS` VARCHAR(20) NULL,
  PRIMARY KEY (`ID`),
  INDEX `PROF_DIR_TESIS_idx` (`DIRECTOR_TESIS` ASC),
  INDEX `PROF_DEPART_idx` (`DEPARTAMENTO` ASC),
  CONSTRAINT `PROF_DIR_TESIS`
    FOREIGN KEY (`DIRECTOR_TESIS`)
    REFERENCES `docencia_relacional`.`profesores` (`ID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `PROF_DEPART`
    FOREIGN KEY (`DEPARTAMENTO`)
    REFERENCES `docencia_relacional`.`departamentos` (`CODIGO`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `docencia_relacional`.`investigadores`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `docencia_relacional`.`investigadores` (
  `ID_PROFESOR` VARCHAR(20) NOT NULL,
  `TRAMOS` INT(2) NULL,
  PRIMARY KEY (`ID_PROFESOR`),
  CONSTRAINT `INVES_PROF`
    FOREIGN KEY (`ID_PROFESOR`)
    REFERENCES `docencia_relacional`.`profesores` (`ID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `docencia_relacional`.`impartir`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `docencia_relacional`.`impartir` (
  `PROFESOR` VARCHAR(20) NOT NULL,
  `ASIGNATURA` INT(3) NOT NULL,
  `GRUPO` VARCHAR(1) NOT NULL,
  `CURSO` VARCHAR(5) NOT NULL,
  `CARGA_CREDITOS` DECIMAL(3,1) NULL,
  PRIMARY KEY (`PROFESOR`, `ASIGNATURA`, `GRUPO`, `CURSO`),
  INDEX `ASIGNATURA_idx` (`ASIGNATURA` ASC),
  CONSTRAINT `IMPAR_ASIG`
    FOREIGN KEY (`ASIGNATURA`)
    REFERENCES `docencia_relacional`.`asignaturas` (`CODIGO`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `IMPAR_PROF`
    FOREIGN KEY (`PROFESOR`)
    REFERENCES `docencia_relacional`.`profesores` (`ID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `docencia_relacional`.`prerrequisito`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `docencia_relacional`.`prerrequisito` (
  `ASIGNATURA` INT(3) NOT NULL,
  `PRERREQUISITO` INT(3) NOT NULL,
  PRIMARY KEY (`ASIGNATURA`, `PRERREQUISITO`),
  INDEX `PRERRE_P_idx` (`PRERREQUISITO` ASC),
  CONSTRAINT `ASIGNATURA_P`
    FOREIGN KEY (`ASIGNATURA`)
    REFERENCES `docencia_relacional`.`asignaturas` (`CODIGO`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `PRERRE_P`
    FOREIGN KEY (`PRERREQUISITO`)
    REFERENCES `docencia_relacional`.`asignaturas` (`CODIGO`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `docencia_relacional`.`matriular`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `docencia_relacional`.`matriular` (
  `ALUMNO` INT(9) NOT NULL,
  `ASIGNATURA` INT(3) NOT NULL,
  `GRUPO` VARCHAR(1) NOT NULL,
  `CURSO` VARCHAR(5) NOT NULL,
  `CALIFICACION` VARCHAR(2) NULL,
  PRIMARY KEY (`ALUMNO`, `ASIGNATURA`, `GRUPO`, `CURSO`),
  INDEX `MATRI_ASIG_idx` (`ASIGNATURA` ASC),
  CONSTRAINT `MATRI_ASIG`
    FOREIGN KEY (`ASIGNATURA`)
    REFERENCES `docencia_relacional`.`asignaturas` (`CODIGO`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `MATRI_ALUM`
    FOREIGN KEY (`ALUMNO`)
    REFERENCES `docencia_relacional`.`alumnos` (`DNI`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
