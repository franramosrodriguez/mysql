
/*GROUP TASK - STUDENTS
Castilla Maldonado, Jorge
Tendero Jiménez, Fco Rafael
Rodríguez Sánchez, Fco Javier
Ramos Rodríguez, Fco Javier
*/

/*QUERY 1:  Number of employees per lodging having a salary higher than the average salary of the rest of employees. Display the name of the lodging.*/
SELECT L.NAME LODGING, COUNT(*) WORKERS
FROM workers W JOIN lodging L ON W.LOD_ID = L.ID 
WHERE SALARY >
	(SELECT AVG(SALARY) FROM workers)
GROUP BY L.NAME;


/*QUERY 2: Pairs of clients that have shared their stay in the same hotel or aparthotel during a period fo time higher than 1 day. Show the full name of each cient.*/
SELECT CONCAT(C1.FNAME,' ', C1.LNAME) 'CLIENT 1', CONCAT(C2.FNAME,' ', C2.LNAME) 'CLIENT 2'
FROM clients C1, book B1, clients C2
WHERE C1.CLIENT_ID = B1.CLIENTS AND C1.CLIENT_ID < C2.CLIENT_ID AND NOT EXISTS
	(SELECT * FROM book B2 WHERE B2.CLIENTS = C2.CLIENT_ID AND B2.LODGING = B1.LODGING
    AND (TIMESTAMPDIFF(DAY, B1.START, B2.DEPART) >= 1 OR TIMESTAMPDIFF(DAY, B2.START, B1.DEPART) >= 1 ));


/*QUERY 3: Show the supplier with the higher sum of prices of its supplied articles.
Show the supplier's name and the entioned quantity. */    
SELECT S.NAME, SUM(A.PRICE) 'SUM OF PRICES'
FROM supplier S, articles A
WHERE S.SP_ID = A.SP_ID
GROUP BY S.SP_ID
HAVING SUM(A.PRICE) =
(SELECT MAX(T.TOTAL) FROM (SELECT SUM(PRICE) TOTAL FROM articles GROUP BY SP_ID)T);



/*QUERY 4: Fin the most requested article.
Show the article's name and the total quantity requested, counting all of the requests.*/
SELECT A.NAME, SUM(O.QUANTITY) 'TOTAL QUANTITY'
FROM articles A NATURAL JOIN hotels.order O 
GROUP BY A.NAME, A.ART_ID
HAVING SUM(O.QUANTITY) = 
	(SELECT MAX(T.TOTAL) FROM (SELECT ART_ID, SUM(QUANTITY) TOTAL FROM hotels.order 
    GROUP BY ART_ID)T);


/*QUERY 5: Workers who started working in the same year but in different lodging */
SELECT CONCAT(W1.FNAME,' ',W1.LNAME) 'WORKER 1', CONCAT(W2.FNAME,' ',W2.LNAME) 'WORKER 2'
FROM workers W1, workers W2
WHERE W1.SSN < W2.SSN AND W1.LOD_ID != W2.LOD_ID AND ABS(TIMESTAMPDIFF(YEAR, W1.START_DATE, W2.START_DATE)) = 1;


/*QUERY 6: Obtain the suppliers not supplying to lodgings with code 4*/
SELECT S.NAME
FROM supplier S, supply SP
WHERE S.SP_ID = SP.SP_ID AND NOT EXISTS  
	(SELECT * FROM supply WHERE SP_ID = S.SP_ID AND LOD_ID = 4);


/*QUERY 7: Show the name of clients, the hotel they stayed and for how long -days- for each reserve, in alphabetical order */
SELECT CONCAT(C.FNAME,' ',C.LNAME) 'CLIENT', L.NAME, TIMESTAMPDIFF(DAY,B.START,B.DEPART) DAYS
FROM clients C JOIN book B ON C.CLIENT_ID = B.CLIENTS JOIN lodging L ON L.ID = B.LODGING
ORDER BY C.LNAME, C.FNAME; 

 
/*QUERY 8: Display the first and last name of workers who also are clients.
A worker is also a client if his firstname, lastname and telephone are identical*/
SELECT c.FNAME,FNAME, w.LNAME
FROM clients c, workers w
WHERE c.FNAME= w.FNAME AND c.LNAME = w.FNAME AND c.PHONE= w.PHONE;


/*QUERY 9: First and last name of pair of workers frm the same hotel or aparthotel where the salary gap is less than 200 euros. */
SELECT w1.fname, w1.lname, w2.fname, w2.lname
FROM workers w1, workers w2
where w1.SSN > w2.SSN 
and abs(w1.salary -w2.salary) >= 200
and w1.LOD_ID = w2.LOD_ID;


/*QUERY 10: Name and description of articles ordered by MALVARROSA aparthotel having  price higher than 20 */
SELECT art.NAME, IFNULL(art.DESCRIPTION, 'No description')
FROM articles art, hotels.order o, lodging a
where art.ART_ID = o.ART_ID
and o.lod_id = a.id and a.name = "MALVARROSA" and art.price > 20
